#ifdef _WIN32
	#include <QtPlatformHeaders/QWindowsWindowFunctions>
#endif

#include "ui_GLMainWindow.h"

#include "window.hpp"
#include "cloth/renderer.hpp"
#include "cloth/simulation.hpp"
#include "fluid/renderer.hpp"
#include "fluid/simulation.hpp"
#include "gravity/renderer.hpp"
#include "gravity/simulation.hpp"
#include "heat/renderer.hpp"
#include "heat/simulation.hpp"


GLMainWindow::GLMainWindow(
cloth::simulation* cloth_sim,
fluid::simulation* fluid_sim,
gravity::simulation* gravity_sim,
heat::simulation* heat_sim,
QWidget * parent,
Qt::WindowFlags f
)
: QMainWindow(parent, f)
, m_ui(new Ui::GLMainWindow())
, m_cloth_sim(cloth_sim)
, m_fluid_sim(fluid_sim)
, m_gravity_sim(gravity_sim)
, m_heat_sim(heat_sim)
, m_next_tab(1)
{
	this->m_ui->setupUi(this);
	this->setWindowTitle(QApplication::applicationDisplayName());

	this->add_tab();
	this->m_ui->sim_gravity_tabs_masses->setTabVisible(1, false);

	this->m_ui->sim_gravity_gl->set_renderer([&](){
		return std::make_unique<gravity::renderer>(&m_timer, m_gravity_sim);
	});
	this->m_ui->sim_heat_gl->set_renderer([&](){
		return std::make_unique<heat::renderer>(&m_timer, m_heat_sim);
	});
	this->m_ui->sim_cloth_gl->set_renderer([&](){
		return std::make_unique<cloth::renderer>(&m_timer, m_cloth_sim);
	});
	this->m_ui->sim_fluid_gl->set_renderer([&](){
		return std::make_unique<fluid::renderer>(&m_timer, m_fluid_sim);
	});

	this->m_gravity_sim->set_g_constant(m_ui->sim_gravity_g->value());
	this->m_gravity_sim->set_m_constant(m_ui->sim_gravity_mass->value());
	this->m_gravity_sim->set_position(0, m_ui->sim_gravity_rx_0->value(), m_ui->sim_gravity_ry_0->value(), m_ui->sim_gravity_rz_0->value());
	this->m_gravity_sim->set_velocity(0, m_ui->sim_gravity_vx_0->value(), m_ui->sim_gravity_vy_0->value(), m_ui->sim_gravity_vz_0->value());
	this->m_gravity_sim->set_mass(0, m_ui->sim_gravity_m_0->value());
	this->m_gravity_sim->reset();

	this->m_cloth_sim->set_fixed(m_ui->sim_cloth_points->isChecked());
	this->m_cloth_sim->set_step_count(m_ui->sim_cloth_steps->value());
	this->m_cloth_sim->set_dt(m_ui->sim_cloth_dt->value());
	this->m_cloth_sim->set_mass(m_ui->sim_cloth_mass->value());
	this->m_cloth_sim->set_struc_ks(m_ui->sim_cloth_struc_ks->value());
	this->m_cloth_sim->set_struc_kd(m_ui->sim_cloth_struc_kd->value());
	this->m_cloth_sim->set_sciss_ks(m_ui->sim_cloth_sciss_ks->value());
	this->m_cloth_sim->set_sciss_kd(m_ui->sim_cloth_sciss_kd->value());
	this->m_cloth_sim->set_stret_ks(m_ui->sim_cloth_stret_ks->value());
	this->m_cloth_sim->set_stret_kd(m_ui->sim_cloth_stret_kd->value());
	this->m_cloth_sim->reset();

	this->m_timer.start();
}

GLMainWindow::~GLMainWindow()
{
}


void GLMainWindow::on_sim_selection_currentChanged(int index)
{
	switch (index) {
	case 0:
		this->m_ui->sim_gravity_gl->on_switch();
		break;
	case 1:
		this->m_ui->sim_heat_gl->on_switch();
		break;
	case 2:
		this->m_ui->sim_cloth_gl->on_switch();
		break;
	case 3:
		this->m_ui->sim_fluid_gl->on_switch();
		break;
	}
}

void GLMainWindow::on_sim_gravity_integration_currentIndexChanged(int index)
{
	this->m_gravity_sim->set_integration_mode(index);
}

void GLMainWindow::on_sim_gravity_g_valueChanged(double g)
{
	this->m_gravity_sim->set_g_constant(g);
}

void GLMainWindow::on_sim_gravity_mass_valueChanged(double m)
{
	this->m_gravity_sim->set_m_constant(m);
}

void GLMainWindow::on_sim_gravity_nbody_clicked(bool value)
{
	if (value) {
		this->m_ui->sim_gravity_mass->setDisabled(true);
		this->m_ui->sim_gravity_add_mass->setDisabled(false);
		this->m_ui->sim_gravity_rmv_mass->setDisabled(false);
		for (int i=1; i<m_ui->sim_gravity_tabs_masses->count(); i++) {
			this->m_ui->sim_gravity_tabs_masses->setTabVisible(i, true);
		}
		this->m_ui->sim_gravity_tabs_masses->setTabVisible(0, false);
	} else {
		this->m_ui->sim_gravity_mass->setDisabled(false);
		this->m_ui->sim_gravity_add_mass->setDisabled(true);
		this->m_ui->sim_gravity_rmv_mass->setDisabled(true);
		for (int i=1; i<m_ui->sim_gravity_tabs_masses->count(); i++) {
			this->m_ui->sim_gravity_tabs_masses->setTabVisible(i, false);
		}
		this->m_ui->sim_gravity_tabs_masses->setTabVisible(0, true);
	}
	this->m_gravity_sim->set_mode(value);
}

void GLMainWindow::on_sim_gravity_add_mass_clicked()
{
	this->add_tab();
}

void GLMainWindow::on_sim_gravity_rmv_mass_clicked()
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	if (m_ui->sim_gravity_tabs_masses->count() > 2) {
		this->m_ui->sim_gravity_tabs_masses->removeTab(selected_tab);
		this->m_gravity_sim->rmv_mass(selected_tab);
	}
}

void GLMainWindow::on_sim_gravity_rx_0_valueChanged(double rx)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* ry = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_ry_" + std::to_string(selected_tab)));
	auto* rz = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_rz_" + std::to_string(selected_tab)));
	if (ry && rz) {
		this->m_gravity_sim->set_position(selected_tab, rx, ry->value(), rz->value());
	}
}

void GLMainWindow::on_sim_gravity_ry_0_valueChanged(double ry)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* rx = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_rx_" + std::to_string(selected_tab)));
	auto* rz = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_rz_" + std::to_string(selected_tab)));
	if (rx && rz) {
		this->m_gravity_sim->set_position(selected_tab, rx->value(), ry, rz->value());
	}
}

void GLMainWindow::on_sim_gravity_rz_0_valueChanged(double rz)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* rx = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_rx_" + std::to_string(selected_tab)));
	auto* ry = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_ry_" + std::to_string(selected_tab)));
	if (rx && ry) {
		this->m_gravity_sim->set_position(selected_tab, rx->value(), ry->value(), rz);
	}
}

void GLMainWindow::on_sim_gravity_vx_0_valueChanged(double vx)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* vy = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vx_" + std::to_string(selected_tab)));
	auto* vz = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vy_" + std::to_string(selected_tab)));
	if (vy && vz) {
		this->m_gravity_sim->set_velocity(selected_tab, vx, vy->value(), vz->value());
	}
}

void GLMainWindow::on_sim_gravity_vy_0_valueChanged(double vy)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* vx = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vx_" + std::to_string(selected_tab)));
	auto* vz = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vz_" + std::to_string(selected_tab)));
	if (vx && vz) {
		this->m_gravity_sim->set_velocity(selected_tab, vx->value(), vy, vz->value());
	}
}

void GLMainWindow::on_sim_gravity_vz_0_valueChanged(double vz)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	auto* vx = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vx_" + std::to_string(selected_tab)));
	auto* vy = m_ui->sim_gravity_tabs_masses->findChild<QDoubleSpinBox*>(QString::fromStdString("sim_gravity_vy_" + std::to_string(selected_tab)));
	if (vx && vy) {
		this->m_gravity_sim->set_velocity(selected_tab, vx->value(), vy->value(), vz);
	}
}

void GLMainWindow::on_sim_gravity_m_0_valueChanged(double m)
{
	int selected_tab = m_ui->sim_gravity_tabs_masses->currentIndex();
	this->m_gravity_sim->set_mass(selected_tab, m);
}

void GLMainWindow::on_sim_gravity_reset_clicked()
{
	this->m_gravity_sim->reset();
}

void GLMainWindow::on_sim_heat_dt_valueChanged(double dt)
{
	this->m_heat_sim->set_dt(dt);
}

void GLMainWindow::on_sim_heat_a_valueChanged(double a)
{
	this->m_heat_sim->set_a(a);
}

void GLMainWindow::on_sim_heat_reset_clicked()
{
	this->m_heat_sim->reset();
}

void GLMainWindow::on_sim_cloth_mode_currentIndexChanged(int n)
{
	this->m_cloth_sim->set_mode(n);
}

void GLMainWindow::on_sim_cloth_points_clicked(bool b)
{
	this->m_cloth_sim->set_fixed(b);
}

void GLMainWindow::on_sim_cloth_steps_valueChanged(int steps)
{
	this->m_cloth_sim->set_step_count(steps);
}

void GLMainWindow::on_sim_cloth_dt_valueChanged(double dt)
{
	this->m_cloth_sim->set_dt(dt);
}

void GLMainWindow::on_sim_cloth_damping_valueChanged(double damping)
{
	this->m_cloth_sim->set_damping(damping);
}

void GLMainWindow::on_sim_cloth_mass_valueChanged(double mass)
{
	this->m_cloth_sim->set_mass(mass);
}

void GLMainWindow::on_sim_cloth_wireframe_clicked(bool w)
{
	this->m_cloth_sim->set_wireframe_mode(w);
}

void GLMainWindow::on_sim_cloth_struc_ks_valueChanged(double ks)
{
	this->m_cloth_sim->set_struc_ks(ks);
}

void GLMainWindow::on_sim_cloth_struc_kd_valueChanged(double kd)
{
	this->m_cloth_sim->set_struc_kd(kd);
}

void GLMainWindow::on_sim_cloth_sciss_ks_valueChanged(double ks)
{
	this->m_cloth_sim->set_sciss_ks(ks);
}

void GLMainWindow::on_sim_cloth_sciss_kd_valueChanged(double kd)
{
	this->m_cloth_sim->set_sciss_kd(kd);
}

void GLMainWindow::on_sim_cloth_stret_ks_valueChanged(double ks)
{
	this->m_cloth_sim->set_stret_ks(ks);
}

void GLMainWindow::on_sim_cloth_stret_kd_valueChanged(double kd)
{
	this->m_cloth_sim->set_stret_kd(kd);
}

void GLMainWindow::on_sim_cloth_reset_clicked()
{
	this->m_cloth_sim->reset();
}

void GLMainWindow::on_sim_fluid_mode_currentIndexChanged(int index)
{
	this->m_fluid_sim->set_mode(index);
}

void GLMainWindow::on_sim_fluid_toggle_view_clicked()
{
	this->m_fluid_sim->toggle_view_mode();
	if (m_fluid_sim->view_mode()) {
		this->m_ui->sim_fluid_label_toggle->setText("displayed value:\npressure");
	} else {
		this->m_ui->sim_fluid_label_toggle->setText("displayed value:\nveclocity");
	}
}

void GLMainWindow::on_sim_fluid_reset_clicked()
{
	this->m_fluid_sim->reset();
}

void GLMainWindow::add_tab()
{
	auto create_spinbox = [](std::string const& name) {
		QDoubleSpinBox* spinbox = new QDoubleSpinBox();
		spinbox->setMinimum(-999.0);
		spinbox->setMaximum( 999.0);
		spinbox->setDecimals(3);
		spinbox->setObjectName(QString::fromStdString(name));
		return spinbox;
	};
	std::string tab_id = std::to_string(m_next_tab);
	this->m_next_tab++;
	QWidget* mass_tab = new QWidget(m_ui->sim_gravity_tabs_masses);
	mass_tab->setObjectName(QString::fromStdString("sim_gravity_tab_mass_" + tab_id));
	QGridLayout* layout = new QGridLayout();
	QDoubleSpinBox* rx = create_spinbox("sim_gravity_rx_" + tab_id);
	QDoubleSpinBox* ry = create_spinbox("sim_gravity_ry_" + tab_id);
	QDoubleSpinBox* rz = create_spinbox("sim_gravity_rz_" + tab_id);
	QDoubleSpinBox* vx = create_spinbox("sim_gravity_vx_" + tab_id);
	QDoubleSpinBox* vy = create_spinbox("sim_gravity_vy_" + tab_id);
	QDoubleSpinBox* vz = create_spinbox("sim_gravity_vz_" + tab_id);
	QDoubleSpinBox* m = create_spinbox("sim_gravity_m_" + tab_id);
	layout->addWidget(new QLabel("r"), 0, 0);
	layout->addWidget(rx, 0, 1);
	layout->addWidget(ry, 0, 2);
	layout->addWidget(rz, 0, 3);
	layout->addWidget(new QLabel("v"), 1, 0);
	layout->addWidget(vx, 1, 1);
	layout->addWidget(vy, 1, 2);
	layout->addWidget(vz, 1, 3);
	layout->addWidget(new QLabel("m"), 2, 0);
	layout->addWidget(m, 2, 1);
	mass_tab->setLayout(layout);
	mass_tab->hide();
	this->m_ui->sim_gravity_tabs_masses->addTab(mass_tab, QString::fromStdString("Mass " + tab_id));

	this->connect(rx, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_rx_0_valueChanged);
	this->connect(ry, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_ry_0_valueChanged);
	this->connect(rz, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_rz_0_valueChanged);
	this->connect(vx, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_vx_0_valueChanged);
	this->connect(vy, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_vy_0_valueChanged);
	this->connect(vz, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_vz_0_valueChanged);
	this->connect(m,  qOverload<double>(&QDoubleSpinBox::valueChanged), this, &GLMainWindow::on_sim_gravity_m_0_valueChanged );

	this->m_gravity_sim->add_mass();
}
