#include <glad/glad.h>
#include <QElapsedTimer>
#include <QMouseEvent>
#include <fstream>
#include <iostream>
#include <Eigen/Geometry>

#include "renderer.hpp"
#include "cloth/simulation.hpp"
#include "fluid/simulation.hpp"
#include "gravity/simulation.hpp"
#include "heat/simulation.hpp"

renderer::renderer(QElapsedTimer* timer)
: m_timer(timer)
, m_last_ns(0)
, m_dragged(false)
, m_yaw(0.0)
, m_pitch(0.0)
, m_distance(5.0)
{
	this->update_view();
}

renderer::~renderer()
{
}


void renderer::resize(int width, int height)
{
	this->m_width = width;
	this->m_height = height;
	if (m_proj_type == PERSPECTIVE) {
		this->m_projection = calc_persp(width, height);
	} else {
		this->m_projection = calc_ortho(width, height);
	}
}

void renderer::on_switch()
{
	this->m_last_ns = m_timer->nsecsElapsed();
}

unsigned int renderer::create_shader(const std::string& path_v, const std::string& path_f) const
{
	unsigned int program_id = ::glCreateProgram();
	GLuint shader_vert = compile_shader(path_v, GL_VERTEX_SHADER);
	GLuint shader_frag = compile_shader(path_f, GL_FRAGMENT_SHADER);
	::glAttachShader(program_id, shader_vert);
	::glAttachShader(program_id, shader_frag);
	::glLinkProgram(program_id);
	int success;
	::glGetProgramiv(program_id, GL_LINK_STATUS, &success);
	if (!success) {
		char log[512];
		::glGetProgramInfoLog(program_id, 512, NULL, log);
		::fprintf(stderr, "%s", log);
		::glDeleteProgram(program_id);
		program_id = 0;
	}
	::glDeleteShader(shader_vert);
	::glDeleteShader(shader_frag);
	return program_id;
}

Eigen::Matrix4d renderer::look_at(const Eigen::Vector3d& pos, const Eigen::Vector3d& target, const Eigen::Vector3d& up) const
{
	Eigen::RowVector3d f = (pos - target).normalized();
	Eigen::RowVector3d s = up.cross(f).normalized();
	Eigen::RowVector3d u = f.cross(s);

	Eigen::Matrix4d v;
	v <<
		s, -s.dot(pos),
		u, -u.dot(pos),
		f, -f.dot(pos),
		Eigen::RowVector4d::UnitW();
	return v;
}

void renderer::update_view()
{
	double ys = std::sin(m_yaw);
	double yc = std::cos(m_yaw);
	double ps = std::sin(m_pitch);
	double pc = std::cos(m_pitch);
	Eigen::Vector3d eye_pos(ps * yc, ps * ys, pc);
	Eigen::Vector3d target(0.0, 0.0, 0.0);
	Eigen::Vector3d up(0.0, 0.0, 1.0);
	this->m_view = look_at(m_distance * eye_pos, target, up);
}

void renderer::check_error() const
{
	GLenum errorCode;
	while ((errorCode = ::glGetError()) != GL_NO_ERROR) {
		std::string error("OpenGL: ");
		switch (errorCode) {
			case GL_INVALID_ENUM:
				error += "INVALID_ENUM";
				break;
			case GL_INVALID_VALUE:
				error += "INVALID_VALUE";
				break;
			case GL_INVALID_OPERATION:
				error += "INVALID_OPERATION";
				break;
			case GL_OUT_OF_MEMORY:
				error += "OUT_OF_MEMORY";
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				error += "INVALID_FRAMEBUFFER_OPERATION";
				break;
			default:
				error += "UNKNOWN ERROR";
				break;
		}
		std::cout << error << std::endl;
	}
}

unsigned int renderer::compile_shader(const std::string& path, unsigned int type) const
{
	std::string shader_code;
	std::ifstream file;
	try {
		file.open(path);
		if (!file.is_open()) {
			return 0;
		} else {
			std::string shader_code((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
			file.close();

			GLuint shader_id = ::glCreateShader(type);
			const char * src_ptr = shader_code.c_str();
			::glShaderSource(shader_id, 1, &src_ptr, NULL);
			::glCompileShader(shader_id);
			int success;
			::glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);

			if(!success) {
				char log[512];
				::glGetShaderInfoLog(shader_id, 512, NULL, log);
			}
			return shader_id;
		}
	} catch(const std::fstream::failure& f) {
		return 0;
	}
}

Eigen::Matrix4d renderer::calc_ortho(int width, int height) const
{
	double left = -0.5 * width;
	double right = 0.5 * width;
	double bottom = -0.5 * height;
	double top = 0.5 * height;
	double ozfar = 1.0;
	double oznear = -1.0;
	Eigen::Matrix4d o;
	o << 2.0 / (right - left), 0, 0, -(right + left) / (right - left),
		 0, 2.0 / (top - bottom), 0, -(top + bottom) / (top - bottom),
		 0, 0, -2.0 / (ozfar - oznear), -(ozfar + oznear) / (ozfar - oznear),
		 0, 0, 0, 1.0;
	return o;
}

Eigen::Matrix4d renderer::calc_persp(int width, int height) const
{
	double ratio = static_cast<double>(width) / height;
	double fov = 0.78539816339744831;
	double z_near = 0.01;

	double range = std::tan(fov / 2);
	double right = ratio >= 1.0 ? range * ratio : range;
	double top = ratio >= 1.0 ? range : range / ratio;

	Eigen::Matrix4d p;
	p <<
		1 / right, 0, 0, 0,
		0, 1 / top, 0, 0,
		0, 0, 0, -2 * z_near,
		0, 0, -1, 0;
	return p;
}

void renderer::on_mouse_event(QMouseEvent* event)
{
	switch (event->type()) {
	case QEvent::MouseButtonPress:
		if (event->button() == Qt::LeftButton) {
			this->m_dragged = true;
			this->m_last_pos = event->pos();
		}
		break;
	case QEvent::MouseButtonRelease:
		if (event->button() == Qt::LeftButton) {
			this->m_dragged = false;
		}
		break;
	default:
		if (m_dragged) {
			QPoint pos = event->pos();
			QPoint diff = pos - m_last_pos;
			this->m_last_pos = event->pos();
			double strength = 2.0 * M_PI / Eigen::Vector2d(m_width, m_height).norm();
			this->m_yaw -= strength * diff.x();
			this->m_yaw = std::fmod(m_yaw, 2.0 * M_PI);
			this->m_pitch -= strength * diff.y();
			this->m_pitch = std::fmax(std::fmin(m_pitch, M_PI - 0.01), 0.01);

			this->update_view();
		}
		return;
	}
}

void renderer::on_wheel_event(QWheelEvent* event)
{
	double distance = m_distance - event->angleDelta().y() / 100.0;
	distance = std::max(0.001, distance);
	this->m_distance = distance;
	this->update_view();
}
