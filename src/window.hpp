#pragma once

#include <memory>
#include <QElapsedTimer>
#include <QMainWindow>

namespace Ui
{
	class GLMainWindow;
} // namespace Ui

namespace cloth {
	class simulation;
} // namespace cloth

namespace fluid {
	class simulation;
} // namespace fluid

namespace gravity {
	class simulation;
} // namespace gravity

namespace heat {
	class simulation;
} // namespace heat


class GLMainWindow : public QMainWindow {
	Q_OBJECT

	private:
		std::unique_ptr<Ui::GLMainWindow> m_ui;
		cloth::simulation* m_cloth_sim;
		fluid::simulation* m_fluid_sim;
		gravity::simulation* m_gravity_sim;
		heat::simulation* m_heat_sim;
		size_t m_next_tab;
		QElapsedTimer m_timer;


	public:
		GLMainWindow(
			cloth::simulation* cloth_sim,
			fluid::simulation* fluid_sim,
			gravity::simulation* gravity_sim,
			heat::simulation* heat_sim,
			QWidget * parent = nullptr,
			Qt::WindowFlags f = Qt::WindowFlags()
		);
		~GLMainWindow();


	public slots:
		void on_sim_selection_currentChanged(int index);

		// gravity simulation
		void on_sim_gravity_integration_currentIndexChanged(int index);
		void on_sim_gravity_g_valueChanged(double g);
		void on_sim_gravity_mass_valueChanged(double m);
		void on_sim_gravity_nbody_clicked(bool value);
		void on_sim_gravity_add_mass_clicked();
		void on_sim_gravity_rmv_mass_clicked();
		void on_sim_gravity_rx_0_valueChanged(double rx);
		void on_sim_gravity_ry_0_valueChanged(double ry);
		void on_sim_gravity_rz_0_valueChanged(double rz);
		void on_sim_gravity_vx_0_valueChanged(double vx);
		void on_sim_gravity_vy_0_valueChanged(double vy);
		void on_sim_gravity_vz_0_valueChanged(double vz);
		void on_sim_gravity_m_0_valueChanged(double vz);
		void on_sim_gravity_reset_clicked();

		// heat simulation
		void on_sim_heat_dt_valueChanged(double dt);
		void on_sim_heat_a_valueChanged(double a);
		void on_sim_heat_reset_clicked();

		// cloth simulation
		void on_sim_cloth_mode_currentIndexChanged(int n);
		void on_sim_cloth_points_clicked(bool b);
		void on_sim_cloth_steps_valueChanged(int steps);
		void on_sim_cloth_dt_valueChanged(double dt);
		void on_sim_cloth_damping_valueChanged(double damping);
		void on_sim_cloth_mass_valueChanged(double mass);
		void on_sim_cloth_wireframe_clicked(bool w);
		void on_sim_cloth_struc_ks_valueChanged(double ks);
		void on_sim_cloth_struc_kd_valueChanged(double kd);
		void on_sim_cloth_sciss_ks_valueChanged(double ks);
		void on_sim_cloth_sciss_kd_valueChanged(double kd);
		void on_sim_cloth_stret_ks_valueChanged(double ks);
		void on_sim_cloth_stret_kd_valueChanged(double kd);
		void on_sim_cloth_reset_clicked();

		// fluid simulation
		void on_sim_fluid_mode_currentIndexChanged(int index);
		void on_sim_fluid_toggle_view_clicked();
		void on_sim_fluid_reset_clicked();

	private:
		void add_tab();
};
