#pragma once

#include <QOpenGLWidget>

class renderer;

class GLWidget : public QOpenGLWidget {
	Q_OBJECT

	private:
		std::unique_ptr<renderer> m_renderer;
		std::function<std::unique_ptr<renderer>(void)> m_rfunc;


	public:
		GLWidget(QWidget * parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
		~GLWidget() = default;


	public:
		void set_renderer(std::function<std::unique_ptr<renderer>(void)> const& rfunc);
		bool event(QEvent * event) override;
		void on_switch();
		void initializeGL() override;
		void paintGL() override;
		void resizeGL(int w, int h) override;
};
