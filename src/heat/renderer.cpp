#include <cmath>
#include <glad/glad.h>
#include <Eigen/Geometry>
#include <QMouseEvent>
#include "renderer.hpp"
#include "simulation.hpp"

static float const column_vertices[12*6] = {
    -0.5f,  0.5f, 1.0f,
     0.5f, -0.5f, 1.0f,
    -0.5f, -0.5f, 1.0f,
     0.5f,  0.5f, 1.0f,

    -0.5f,  0.5f, 0.0f,
     0.5f, -0.5f, 0.0f,
    -0.5f, -0.5f, 0.0f,
     0.5f,  0.5f, 0.0f,

    -0.5f,  0.5f, 0.0f,
    -0.5f, -0.5f, 1.0f,
    -0.5f, -0.5f, 0.0f,
    -0.5f,  0.5f, 1.0f,

     0.5f,  0.5f, 0.0f,
     0.5f, -0.5f, 1.0f,
     0.5f, -0.5f, 0.0f,
     0.5f,  0.5f, 1.0f,

     0.5f, -0.5f, 0.0f,
    -0.5f, -0.5f, 1.0f,
    -0.5f, -0.5f, 0.0f,
     0.5f, -0.5f, 1.0f,

     0.5f,  0.5f, 0.0f,
    -0.5f,  0.5f, 1.0f,
    -0.5f,  0.5f, 0.0f,
     0.5f,  0.5f, 1.0f
};

static GLubyte const column_indices[6*6] = {
     0,  1,  2,  0,  3,  1,
     4,  5,  6,  4,  7,  5,
     8,  9, 10,  8, 11,  9,
    12, 13, 14, 12, 15, 13,
    16, 17, 18, 16, 19, 17,
    20, 21, 22, 20, 23, 21
};


heat::renderer::renderer(QElapsedTimer* timer, heat::simulation* heat_sim)
: ::renderer(timer)
, m_heat_sim(heat_sim)
{
	this->m_proj_type = PERSPECTIVE;
	this->m_shader = renderer::create_shader("assets/shaders/heat.vert", "assets/shaders/heat.frag");

	this->m_indices.reserve(heat::simulation::size * heat::simulation::size * 6 * 6);
	for (int y=0; y<heat::simulation::size; y++) {
		for (int x=0; x<heat::simulation::size; x++) {
			GLuint offset = (y*heat::simulation::size + x) * 24;
			for (int i=0; i<36; i++) {
				this->m_indices.push_back(offset + column_indices[i]);
			}
		}
	}

	::glGenVertexArrays(1, &m_vao);
	::glGenBuffers(1, &m_vbo);
	::glGenBuffers(1, &m_ebo);

	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	::glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(m_indices[0]) * m_indices.size()), m_indices.data(), GL_STATIC_DRAW);
	::glBindVertexArray(0);

	this->m_distance = 100.0;
	this->m_yaw = 0.0;
	this->m_pitch = 0.7;
	this->update_view();
}

heat::renderer::~renderer()
{
	::glDeleteBuffers(1, &m_ebo);
	::glDeleteBuffers(1, &m_vbo);
	::glDeleteVertexArrays(1, &m_vao);
	::glDeleteProgram(m_shader);
}


void heat::renderer::render()
{
	::glDisable(GL_CULL_FACE);
	::glEnable(GL_DEPTH_TEST);

	std::vector<float> vertices;
	vertices.reserve(heat::simulation::size * heat::simulation::size * 16 * 6);
	for (int y=0; y<heat::simulation::size; ++y) {
		for (int x=0; x<heat::simulation::size; ++x) {
			float value = m_heat_sim->get_value(x,y);
			float fac = (value/m_heat_sim->maximum);
			for (int i=0; i<24; i++) {
				vertices.push_back(column_vertices[i*3 + 0] + (x - heat::simulation::size/2.0f));
				vertices.push_back(column_vertices[i*3 + 1] + (y - heat::simulation::size/2.0f));
				vertices.push_back(column_vertices[i*3 + 2] * fac * 10);
				vertices.push_back(value);
			}
		}
	}

	int loc = -1;
	::glUseProgram(m_shader);
	loc = ::glGetUniformLocation(m_shader, "projection");
	::glUniformMatrix4fv(loc, 1, GL_FALSE, (m_projection * m_view).cast<float>().eval().data());

	loc = ::glGetUniformLocation(m_shader, "maximum");
	::glUniform1f(loc, m_heat_sim->maximum);

	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(vertices[0]) * vertices.size()), vertices.data(), GL_DYNAMIC_DRAW);
	::glEnableVertexAttribArray(0);
	::glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
	::glBindVertexArray(0);

	Eigen::Affine3d model = Eigen::Affine3d::Identity();
	loc = ::glGetUniformLocation(m_shader, "model");
	::glUniformMatrix4fv(loc, 1, GL_FALSE, model.matrix().cast<float>().eval().data());

	::glBindVertexArray(m_vao);
	::glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
	::glBindVertexArray(0);

	this->m_heat_sim->update();
	this->update();
}
