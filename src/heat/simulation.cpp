#include "simulation.hpp"

typedef Eigen::Triplet<double> triplet;
constexpr int a_size = heat::simulation::size - 2;

heat::simulation::simulation()
: m_a(1.0)
, m_dt(1.0)
, m_coeffs(a_size*a_size, a_size*a_size)
, m_b(a_size*a_size)
{
	std::vector<triplet> triplets(a_size * 5);
	for (int y=1; y<=a_size; ++y) {
		for (int x=1; x<=a_size; ++x) {
			int index = (y-1) * a_size + (x-1);
			triplets.push_back(triplet(index, index, -4.0));
			if (x != 1) {
				triplets.push_back(triplet(index, index - 1, 1.0));
			}
			if (x != a_size) {
				triplets.push_back(triplet(index, index + 1, 1.0));
			}
			if (y != 1) {
				triplets.push_back(triplet(index, index - a_size, 1.0));
			}
			if (y != a_size) {
				triplets.push_back(triplet(index, index + a_size, 1.0));
			}
		}
	}
	this->m_coeffs.setZero();
	this->m_coeffs.setFromTriplets(triplets.begin(), triplets.end());
	this->reset();
}


void heat::simulation::reset()
{
    this->m_b.setZero();
    this->m_b((a_size * a_size) / 2) = maximum;
    Eigen::SparseMatrix<double> I(a_size*a_size, a_size*a_size);
    I.setIdentity();
    this->m_solver.compute(I - m_a * m_dt * m_coeffs);
}

void heat::simulation::set_dt(double dt)
{
	this->m_dt = dt;
}

void heat::simulation::set_a(double a)
{
	this->m_a = a;
}

void heat::simulation::update()
{
	this->m_b = m_solver.solve(m_b).eval();
}

double heat::simulation::get_value(int x, int y) const
{
	if (x <= 0 || y <= 0 || x >= size-1 || y >= size-1) {
		return 0.0;
	} else {
		return m_b((x-1) * (a_size) + (y-1));
	}
}
