#pragma once

#include <Eigen/Sparse>

namespace heat {

class simulation {

	public:
		constexpr static int size = 121;
		constexpr static double maximum = 1;

	private:
		double m_a;
        double m_dt;
        Eigen::SparseMatrix<double> m_coeffs;
        Eigen::VectorXd m_b;
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> m_solver;


	public:
		simulation();
		~simulation() = default;


	public:
		void reset();
		void set_dt(double dt);
		void set_a(double a);
		void update();
		double get_value(int x, int y) const;
};

} // namespace heat
