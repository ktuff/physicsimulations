#include "mass.hpp"
#include "spring.hpp"

cloth::mass::mass(int x, int y)
: m_x(x)
, m_y(y)
, m_position(0.0, 0.0, 0.0)
, m_velocity(0.0, 0.0, 0.0)
, m_mass(0.05)
{
}

cloth::mass::~mass()
{
	for (auto* s : m_springs) {
		s->on_mass_destruction(this);
	}
}


void cloth::mass::add_spring(cloth::spring* s)
{
	this->m_springs.emplace(s);
}

void cloth::mass::remove_spring(cloth::spring* s)
{
	this->m_springs.erase(s);
}

Eigen::Vector3d cloth::mass::get_position() const
{
	return m_position;
}

Eigen::Vector3d cloth::mass::get_velocity() const
{
	return m_velocity;
}

void cloth::mass::set_position(const Eigen::Vector3d& pos)
{
	this->m_position = pos;
}

void cloth::mass::set_velocity(const Eigen::Vector3d& vel)
{
	this->m_velocity = vel;
}

void cloth::mass::set_mass(double mass)
{
	this->m_mass = mass;
}

Eigen::Vector3d cloth::mass::acceleration() const
{
	// gravitational force ~ 10 m/s^2
	Eigen::Vector3d f(0.0, 0.0, -10.0);
	// spring forces
	for (spring const* s : m_springs) {
		f += s->force(this) / m_mass;
	}
	return f;
}

void cloth::mass::acceleration(std::vector<triplet>& K_triplets, std::vector<triplet>& D_triplets, int size) const
{
	int index = m_x + size * m_y;
	Eigen::Matrix3d k_diag;
	Eigen::Matrix3d d_diag;
	k_diag.setZero();
	d_diag.setZero();
	for (auto const* s : m_springs) {
		Eigen::Matrix3d k_mat = s->k_mat(this);
		Eigen::Matrix3d d_mat = s->d_mat(this);
		k_diag -= k_mat;
		d_diag -= d_mat;
		for (int j_KD = 0; j_KD < 3; j_KD++) {
			for (int i_KD = 0; i_KD < 3; i_KD++) {
				int x2 = s->get_m2(this)->m_x;
				int y2 = s->get_m2(this)->m_y;
				int index2 = x2 + size * y2;
				K_triplets.push_back(triplet(index*3 + i_KD, index2*3 + j_KD, k_mat(i_KD,j_KD)));
				D_triplets.push_back(triplet(index*3 + i_KD, index2*3 + j_KD, d_mat(i_KD, j_KD)));
			}
		}
	}

	for (int j_KD = 0; j_KD < 3; j_KD++) {
		for (int i_KD = 0; i_KD < 3; i_KD++) {
			K_triplets.push_back(triplet(index * 3 + i_KD, index * 3 + j_KD, k_diag(i_KD, j_KD)));
			D_triplets.push_back(triplet(index * 3 + i_KD, index * 3 + j_KD, d_diag(i_KD, j_KD)));
		}
	}
}
