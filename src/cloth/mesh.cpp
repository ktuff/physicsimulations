#include "mass.hpp"
#include "mesh.hpp"

double const cloth::mesh::dist = 0.1;

cloth::mesh::mesh()
{
	for (int y=0; y<size; y++) {
		for (int x=0; x<size; x++) {
			this->m_masses[y][x] = std::make_unique<mass>(x, y);
		}
	}

	for (int y=0; y<size; y++) {
		for (int x=0; x<size; x++) {
			mass * m1 = get_mass(x, y);
			// structure
			if (mass * m2 = get_mass(x+1, y)) {
				this->m_springs.push_back(std::make_unique<spring>(STRUCTURE, m1, m2, dist));
			}
			if (mass * m2 = get_mass(x, y+1)) {
				this->m_springs.push_back(std::make_unique<spring>(STRUCTURE, m1, m2, dist));
			}
			// scissor
			if (mass * m2 = get_mass(x+1, y+1)) {
				this->m_springs.push_back(std::make_unique<spring>(SCISSOR, m1, m2, std::sqrt(dist*dist + dist*dist)));
			}
			if (mass * m2 = get_mass(x-1, y+1)) {
				this->m_springs.push_back(std::make_unique<spring>(SCISSOR, m1, m2, std::sqrt(dist*dist + dist*dist)));
			}
			// stretch
			if (mass * m2 = get_mass(x+2, y)) {
				this->m_springs.push_back(std::make_unique<spring>(STRETCH, m1, m2, 2.0*dist));
			}
			if (mass * m2 = get_mass(x, y+2)) {
				this->m_springs.push_back(std::make_unique<spring>(STRETCH, m1, m2, 2.0*dist));
			}
		}
	}
	this->set_mass(1.0);
	this->reset();
}

cloth::mesh::~mesh()
{
	this->m_springs.clear();
}


void cloth::mesh::set_mass(double mass)
{
	for (int y=0; y<size; y++) {
		for (int x=0; x<size; x++) {
			this->m_masses[y][x]->set_mass(mass);
		}
	}
}

void cloth::mesh::set_ks(spring_type type, double ks)
{
	for (auto& s : m_springs) {
		if (s->m_type == type) {
			s->set_ks(ks);
		}
	}
}

void cloth::mesh::set_kd(spring_type type, double kd)
{
	for (auto& s : m_springs) {
		if (s->m_type == type) {
			s->set_kd(kd);
		}
	}
}

void cloth::mesh::set_l0(spring_type type, double l0)
{
	for (auto& s : m_springs) {
		if (s->m_type == type) {
			s->set_l0(l0);
		}
	}
}

void cloth::mesh::reset()
{
	Eigen::Vector3d vel(0.0, 0.0, 0.0);
	for (int y=0; y<size; y++) {
		for (int x=0; x<size; x++) {
			Eigen::Vector3d pos(y * dist, x * dist, 0.0);
			this->m_masses[y][x]->set_position(pos);
			this->m_masses[y][x]->set_velocity(vel);
		}
	}
}

Eigen::Vector3d cloth::mesh::get_position(int x, int y) const
{
	if (x < 0 || y < 0 || x >= size || y >= size) {
		return Eigen::Vector3d(0, 0, 0);
	} else {
		return m_masses[y][x]->get_position();
	}
}

cloth::mass * cloth::mesh::get_mass(int x, int y)
{
	if (x < 0 || y < 0 || x >= size || y >= size) {
		return nullptr;
	} else {
		return m_masses[y][x].get();
	}
}
