#pragma once

#include <memory>
#include <vector>
#include <Eigen/Core>
#include "spring.hpp"

namespace cloth {

class mass;

class mesh {

	public:
		static int const size = 34;
		static double const dist;

	private:
		std::unique_ptr<mass> m_masses[size][size];
		std::vector<std::unique_ptr<spring>> m_springs;


	public:
		mesh();
		mesh(mesh const&) = delete;
		~mesh();


	public:
		void set_mass(double mass);
		void set_ks(spring_type type, double ks);
		void set_kd(spring_type type, double kd);
		void set_l0(spring_type type, double l0);
		void reset();
		Eigen::Vector3d get_position(int x, int y) const;
		mass* get_mass(int x, int y);
};

} // namespace cloth
