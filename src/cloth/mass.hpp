#pragma once

#include <set>
#include <vector>
#include <Eigen/Sparse>

typedef Eigen::Triplet<double> triplet;

namespace cloth {

class spring;

class mass {

	private:
		int const m_x, m_y;
		std::set<spring*> m_springs;
		Eigen::Vector3d m_position;
		Eigen::Vector3d m_velocity;
		double m_mass;


	public:
		mass(int x, int y);
		~mass();


	public:
		void add_spring(spring *s);
		void remove_spring(spring *s);
		Eigen::Vector3d get_position() const;
		Eigen::Vector3d get_velocity() const;
		void set_position(Eigen::Vector3d const& pos);
		void set_velocity(Eigen::Vector3d const& vel);
		void set_mass(double mass);
		Eigen::Vector3d acceleration() const;
		void acceleration(std::vector<triplet>& K_triplets, std::vector<triplet>& D_triplets, int size) const;
};

} // namespace cloth

