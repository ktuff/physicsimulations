#pragma once

#include <Eigen/Sparse>
#include "mesh.hpp"

typedef Eigen::Triplet<double> triplet;

namespace cloth {

class simulation {

	private:
		enum {
			FORWARD,
			BACKWARD
		} m_mode;
		bool m_two_fixed;
		int m_steps;
		double m_dt;
		double m_damping;
		mesh m_mesh;
		double m_mass;
		std::vector<triplet> m_k_triplets;
		std::vector<triplet> m_d_triplets;
		Eigen::SparseMatrix<double> m_mat_m;
		bool m_wireframe;


	public:
		simulation();
		~simulation();


	public:
		void reset();
		void set_mode(int mode);
		void set_fixed(bool two);
		void set_dt(double dt);
		void set_step_count(int steps);
		void set_mass(double mass);
		void set_damping(double damping);
		void set_struc_ks(double value);
		void set_struc_kd(double value);
		void set_sciss_ks(double value);
		void set_sciss_kd(double value);
		void set_stret_ks(double value);
		void set_stret_kd(double value);
		void set_wireframe_mode(bool value);
		bool wireframe_mode() const;
		Eigen::Vector3d get_position(int x, int y) const;
		void update(long dt);

	private:
		void update_forward(long dt);
		void update_backward(long dt);
};

} // namespace cloth
