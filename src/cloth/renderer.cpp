#include <glad/glad.h>
#include <Eigen/Geometry>
#include "renderer.hpp"
#include "simulation.hpp"

cloth::renderer::renderer(QElapsedTimer* timer, cloth::simulation* cloth_sim)
: ::renderer(timer)
, m_cloth_sim(cloth_sim)
{
	this->m_proj_type = PERSPECTIVE;
	this->m_shader = renderer::create_shader("assets/shaders/cloth.vert", "assets/shaders/cloth.frag");

	int const n = cloth::mesh::size;
	this->m_indices.reserve(n * n * 6);
	for (int y=0; y<n-1; y++) {
		for (int x=0; x<n-1; x++) {
			this->m_indices.push_back(y*n + x);
			this->m_indices.push_back((y+1)*n + x + 1);
			this->m_indices.push_back(y*n + x + 1);
			this->m_indices.push_back(y*n + x);
			this->m_indices.push_back((y+1)*n + x);
			this->m_indices.push_back((y+1)*n + x + 1);
		}
	}

	::glGenVertexArrays(1, &m_vao);
	::glGenBuffers(1, &m_vbo);
	::glGenBuffers(1, &m_ebo);

	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	::glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(m_indices[0]) * m_indices.size()), m_indices.data(), GL_STATIC_DRAW);
	::glBindVertexArray(0);

	this->m_distance = 5.0;
	this->m_yaw = 0.7;
	this->m_pitch = 0.7;
	this->update_view();
}

cloth::renderer::~renderer()
{
	::glDeleteBuffers(1, &m_ebo);
	::glDeleteBuffers(1, &m_vbo);
	::glDeleteVertexArrays(1, &m_vao);
	::glDeleteProgram(m_shader);
}


void cloth::renderer::render()
{
	::glDisable(GL_CULL_FACE);
	if (m_cloth_sim->wireframe_mode()) {
		::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else {
		::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	int const n = cloth::mesh::size;
	std::vector<float> vertices;
	vertices.reserve(n * n * 4 * 4);
	for (int y=0; y<n; ++y) {
		for (int x=0; x<n; ++x) {
			Eigen::Vector3d pos = m_cloth_sim->get_position(x, y);
			vertices.push_back(pos.x() - n*0.1f/2.0f);
			vertices.push_back(pos.y() - n*0.1f/2.0f);
			vertices.push_back(pos.z());
			vertices.push_back((y*n + x) / (float)(n*n));
		}
	}

	int loc = -1;
	::glUseProgram(m_shader);
	loc = ::glGetUniformLocation(m_shader, "projection");
	::glUniformMatrix4fv(loc, 1, GL_FALSE, (m_projection * m_view).cast<float>().eval().data());

	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(vertices[0]) * vertices.size()), NULL, GL_DYNAMIC_DRAW);
	::glBufferSubData(GL_ARRAY_BUFFER, 0, static_cast<GLsizeiptr>(sizeof(vertices[0]) * vertices.size()), vertices.data());
	::glEnableVertexAttribArray(0);
	::glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
	::glBindVertexArray(0);

	Eigen::Affine3d model = Eigen::Affine3d::Identity();
	loc = ::glGetUniformLocation(m_shader, "model");
	::glUniformMatrix4fv(loc, 1, GL_FALSE, (model.matrix()).cast<float>().eval().data());

	::glBindVertexArray(m_vao);
	::glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
	::glBindVertexArray(0);

	this->m_cloth_sim->update(1);
	this->update();
}
