#include "mass.hpp"
#include "simulation.hpp"

cloth::simulation::simulation()
: m_mode(FORWARD)
, m_two_fixed(false)
, m_steps(1)
, m_dt(0.001)
, m_damping(0.0)
, m_mat_m(mesh::size * mesh::size * 3, mesh::size * mesh::size * 3)
, m_wireframe(true)
{
	this->m_k_triplets.reserve(mesh::size * mesh::size * 3 * mesh::size * mesh::size * 3);
	this->m_d_triplets.reserve(mesh::size * mesh::size * 3 * mesh::size * mesh::size * 3);

	this->set_mass(1.0);
}

cloth::simulation::~simulation()
{
}


void cloth::simulation::reset()
{
	this->m_mesh.reset();
}

void cloth::simulation::set_mode(int mode)
{
	switch (mode) {
	case 0:
		this->m_mode = FORWARD;
		break;
	case 1:
		this->m_mode = BACKWARD;
		break;
	}
}

void cloth::simulation::set_fixed(bool two)
{
	this->m_two_fixed = two;
}

void cloth::simulation::set_dt(double dt)
{
	this->m_dt = dt;
}

void cloth::simulation::set_step_count(int steps)
{
	this->m_steps = steps;
}

void cloth::simulation::set_mass(double mass)
{
	this->m_mass = mass;
	this->m_mat_m.setIdentity();
	this->m_mat_m = m_mat_m * mass;
}

void cloth::simulation::set_damping(double damping)
{
	this->m_damping = damping;
}

void cloth::simulation::set_struc_ks(double value)
{
	this->m_mesh.set_ks(cloth::STRUCTURE, value);
}

void cloth::simulation::set_struc_kd(double value)
{
	this->m_mesh.set_kd(cloth::STRUCTURE, value);
}

void cloth::simulation::set_sciss_ks(double value)
{
	this->m_mesh.set_ks(cloth::SCISSOR, value);
}

void cloth::simulation::set_sciss_kd(double value)
{
	this->m_mesh.set_kd(cloth::SCISSOR, value);
}

void cloth::simulation::set_stret_ks(double value)
{
	this->m_mesh.set_ks(cloth::STRETCH, value);
}

void cloth::simulation::set_stret_kd(double value)
{
	this->m_mesh.set_kd(cloth::STRETCH, value);
}

void cloth::simulation::set_wireframe_mode(bool value)
{
	this->m_wireframe = value;
}

bool cloth::simulation::wireframe_mode() const
{
	return m_wireframe;
}

Eigen::Vector3d cloth::simulation::get_position(int x, int y) const
{
	return m_mesh.get_position(x, y);
}

void cloth::simulation::update(long dt)
{
	for (int i=0; i<m_steps; i++) {
		switch (m_mode) {
		case FORWARD:
			this->update_forward(dt);
			break;
		case BACKWARD:
			this->update_backward(dt);
			break;
		}
	}
}

void cloth::simulation::update_forward(long dt)
{
	int const n = mesh::size;
	Eigen::Vector3d pos[n][n];
	Eigen::Vector3d vel[n][n];
	Eigen::Vector3d acc[n][n];

	#pragma omp parallel
    {
    #pragma omp for collapse(1)
	for (int y=0; y<n; y++) {
		for (int x=0; x<n; x++) {
			mass * m = m_mesh.get_mass(x, y);
			pos[y][x] = m->get_position();
			vel[y][x] = m->get_velocity();
			acc[y][x] = m->acceleration();
		}
	}
	#pragma omp for collapse(1)
	for (int y=0; y<n; y++) {
		for (int x=0; x<n; x++) {
			mass * m = m_mesh.get_mass(x, y);
			if ((x == 0 && y == 0) || (m_two_fixed && x == n-1 && y == 0)) {
			} else {
				m->set_position(pos[y][x] + m_dt/2.0*vel[y][x]);
				m->set_velocity(vel[y][x] + m_dt/2.0*acc[y][x]);
			}
		}
	}
    #pragma omp for collapse(1)
	for (int y=0; y<n; y++) {
		for (int x=0; x<n; x++) {
			mass * m = m_mesh.get_mass(x, y);
			if ((x == 0 && y == 0) || (m_two_fixed && x == n-1 && y == 0)) {
			} else {
				pos[y][x] = pos[y][x] + m_dt * m->get_velocity();
				vel[y][x] = (1.0 - m_damping) * vel[y][x] + m_dt * m->acceleration();
			}
		}
	}
	#pragma omp for collapse(1)
	for (int y=0; y<n; y++) {
		for (int x=0; x<n; x++) {
			mass * m = m_mesh.get_mass(x, y);
			m->set_position(pos[y][x]);
			m->set_velocity(vel[y][x]);
		}
	}
    }
}

void cloth::simulation::update_backward(long dt)
{
	// setup matrices
	int const n = mesh::size;
	this->m_k_triplets.clear();
	this->m_d_triplets.clear();
	Eigen::SparseMatrix<double> mat_k(n*n*3, n*n*3);
	Eigen::SparseMatrix<double> mat_d(n*n*3, n*n*3);
	Eigen::VectorXd f(n*n*3);
	Eigen::VectorXd v(n*n*3);
	for (int y = 0; y < n; y++) {
		for (int x = 0; x < n; x++) {
			int mass_index = x + n * y;
			mass* m = m_mesh.get_mass(x, y);
			Eigen::Vector3d acc = m->acceleration() * m_mass;
			Eigen::Vector3d vel = m->get_velocity();
			if ((x == 0 && y == 0) || (m_two_fixed && x == n-1 && y == 0)) {
				for (int k = 0; k < 3; k++) {
					f(mass_index * 3 + k) = 0.0;
					v(mass_index * 3 + k) = 0.0;
				}
			} else {
				for (int k = 0; k < 3; k++) {
					f(mass_index * 3 + k) = acc(k);
					v(mass_index * 3 + k) = vel(k);
				}
				m->acceleration(m_k_triplets, m_d_triplets, n);
			}
		}
	}
	mat_k.setFromTriplets(m_k_triplets.begin(), m_k_triplets.end());
	mat_d.setFromTriplets(m_d_triplets.begin(), m_d_triplets.end());
	Eigen::SparseMatrix<double> left = m_mat_m - m_dt * mat_d - std::pow(m_dt, 2) * mat_k;
	Eigen::VectorXd right = m_dt * (f + m_dt * mat_k * v);

	// solve
	Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
	solver.compute(left);
	Eigen::VectorXd dvel = solver.solve(right).eval();

	// update
	for (int j = 0; j<n; j++) {
		for (int i = 0; i<n; i++) {
			int mass_index = i + n*j;
			mass* m = m_mesh.get_mass(i, j);
			Eigen::Vector3d vd(dvel(mass_index*3+0), dvel(mass_index*3+1), dvel(mass_index*3+2));
			Eigen::Vector3d vel = (1.0 - m_damping) * m->get_velocity() + vd;
			Eigen::Vector3d pos = m->get_position() + m_dt * vel;
			if (!((i == 0 && j == 0) || (m_two_fixed && i == n-1 && j == 0))) {
				m->set_position(pos);
				m->set_velocity(vel);
			}
		}
	}
}
