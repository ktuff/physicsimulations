#include "mass.hpp"
#include "spring.hpp"

cloth::spring::spring(spring_type type, mass *m1, mass *m2, double l0)
: m_type(type)
, m_m1(m1)
, m_m2(m2)
, m_ks(0.0)
, m_kd(0.0)
, m_l0(l0)
{
	m1->add_spring(this);
	m2->add_spring(this);
}

cloth::spring::~spring()
{
	if (m_m1) {
		this->m_m1->remove_spring(this);
	}
	if (m_m2) {
		this->m_m2->remove_spring(this);
	}
}


void cloth::spring::on_mass_destruction(cloth::mass* m)
{
	if (m == m_m1) {
		this->m_m1 = nullptr;
	} else if (m == m_m2) {
		this->m_m2 = nullptr;
	}
}

void cloth::spring::set_ks(double ks)
{
	this->m_ks = ks;
}

void cloth::spring::set_kd(double kd)
{
	this->m_kd = kd;
}

void cloth::spring::set_l0(double l0)
{
	this->m_l0 = l0;
}

cloth::mass * cloth::spring::get_m2(const cloth::mass* m) const
{
	if (m == m_m1) {
		return m_m2;
	} else if (m == m_m2) {
		return m_m1;
	} else {
		return nullptr;
	}
}

Eigen::Vector3d cloth::spring::force(cloth::mass const* m) const
{
	if (m != m_m1 && m != m_m2) {
		return Eigen::Vector3d::Zero();
	}
	mass * m1 = (m_m1 == m) ? m_m1 : m_m2;
	mass * m2 = (m_m1 == m) ? m_m2 : m_m1;

	Eigen::Vector3d x12 = m2->get_position() - m1->get_position();
	Eigen::Vector3d v12 = m2->get_velocity() - m1->get_velocity();
	Eigen::Vector3d xn = x12 / x12.norm();
	double dx = x12.norm() - m_l0;
	return (m_ks * dx + m_kd * v12.dot(xn)) * xn;
}

Eigen::Matrix3d cloth::spring::k_mat(cloth::mass const* m) const
{
	if (m != m_m1 && m != m_m2) {
		return Eigen::Matrix3d::Zero();
	}
	mass * m1 = (m_m1 == m) ? m_m1 : m_m2;
	mass * m2 = (m_m1 == m) ? m_m2 : m_m1;

	Eigen::Vector3d x12 = m2->get_position() - m1->get_position();
	double xnorm = x12.norm();
	Eigen::Matrix3d f_el = m_ks * (xnorm - m_l0) / xnorm * Eigen::Matrix3d::Identity()
						 + m_l0 * (x12*x12.transpose()) / std::pow(xnorm, 3);
	return f_el;
}

Eigen::Matrix3d cloth::spring::d_mat(cloth::mass const* m) const
{
	if (m != m_m1 && m != m_m2) {
		return Eigen::Matrix3d::Zero();
	}
	mass * m1 = (m_m1 == m) ? m_m1 : m_m2;
	mass * m2 = (m_m1 == m) ? m_m2 : m_m1;

	Eigen::Vector3d x12 = m2->get_position() - m1->get_position();
	double xnorm = x12.norm();
	Eigen::Vector3d xn = x12 / xnorm;
	return m_kd * xn.dot(xn) * Eigen::Matrix3d::Identity();
}
