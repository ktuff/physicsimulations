#pragma once

namespace cloth {

class mass;

enum spring_type {
	STRUCTURE,
	SCISSOR,
	STRETCH
};

class spring {

	public:
		spring_type const m_type;

	private:
		mass *m_m1;
		mass *m_m2;
		double m_ks;
		double m_kd;
		double m_l0;


	public:
		spring(spring_type type, mass *m1, mass *m2, double l0 = 0.0);
		~spring();


	public:
		void on_mass_destruction(mass *m);
		void set_ks(double ks);
		void set_kd(double kd);
		void set_l0(double l0);
		mass * get_m2(mass const* m) const;
		Eigen::Vector3d force(mass const *m) const;
		Eigen::Matrix3d k_mat(mass const *m) const;
		Eigen::Matrix3d d_mat(mass const *m) const;
};

} // namespace cloth
