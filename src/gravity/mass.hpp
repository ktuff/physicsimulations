#pragma once

#include <Eigen/Core>

namespace gravity {

class mass {

	private:
		double m_m;
		Eigen::Vector3d m_r;
		Eigen::Vector3d m_v;


	public:
		mass();
		~mass() = default;


	public:
		Eigen::Vector3d get_position() const;
		Eigen::Vector3d get_velocity() const;
		double get_mass() const;
		mass* set_position(Eigen::Vector3d const& r);
		mass* set_velocity(Eigen::Vector3d const& v);
		mass* set_mass(double value);
};

} // namespace gravity
