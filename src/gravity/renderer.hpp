#pragma once

#include "../renderer.hpp"

namespace gravity {

class simulation;

class renderer : public ::renderer {

	private:
		simulation *m_gravity_sim;
		unsigned int m_shader;
		unsigned int m_vao;
		unsigned int m_vbo;
		unsigned int m_ebo;
		unsigned int m_tex;


	public:
		renderer(QElapsedTimer* timer, simulation* gravity_sim);
		~renderer();


	public:
		void render() override;
};

} // namespace gravity
