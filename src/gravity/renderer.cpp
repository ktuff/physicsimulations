#include <glad/glad.h>
#include <QElapsedTimer>
#include <QImage>
#include <Eigen/Geometry>

#include "renderer.hpp"
#include "simulation.hpp"

const float cube_vertices[120] = {
	// positions
	-1.0f,  1.0f, -1.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
	 1.0f,  1.0f, -1.0f, 1.0f, 1.0f,

	-1.0f, -1.0f,  1.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
	-1.0f,  1.0f, -1.0f, 1.0f, 0.0f,
	-1.0f,  1.0f,  1.0f, 1.0f, 1.0f,

	 1.0f, -1.0f, -1.0f, 0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f, 1.0f, 0.0f,
	 1.0f,  1.0f, -1.0f, 1.0f, 1.0f,

	-1.0f, -1.0f,  1.0f, 0.0f, 1.0f,
	-1.0f,  1.0f,  1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f, 1.0f, 0.0f,
	 1.0f, -1.0f,  1.0f, 1.0f, 1.0f,

	-1.0f,  1.0f, -1.0f, 0.0f, 1.0f,
	 1.0f,  1.0f, -1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f, 1.0f, 0.0f,
	-1.0f,  1.0f,  1.0f, 1.0f, 1.0f,

	-1.0f, -1.0f, -1.0f, 0.0f, 1.0f,
	-1.0f, -1.0f,  1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
	 1.0f, -1.0f,  1.0f, 1.0f, 1.0f
};

const unsigned int cube_indices[36] = {
     0,  1,  2,  2,  3,  0,
     4,  5,  6,  6,  7,  4,
     8,  9, 10, 10, 11,  8,
    12, 13, 14, 14, 15, 12,
    16, 17, 18, 18, 19, 16,
    20, 21, 22, 22, 21, 23
};

gravity::renderer::renderer(QElapsedTimer* timer, gravity::simulation* gravity_sim)
: ::renderer(timer)
, m_gravity_sim(gravity_sim)
{
	this->m_proj_type = PERSPECTIVE;
	this->m_shader = renderer::create_shader("assets/shaders/gravity.vert", "assets/shaders/gravity.frag");

	::glGenVertexArrays(1, &m_vao);
	::glGenBuffers(1, &m_vbo);
	::glGenBuffers(1, &m_ebo);
	::glGenTextures(1, &m_tex);

	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
	::glEnableVertexAttribArray(0);
	::glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*5, (void*)(sizeof(float)*0));
	::glEnableVertexAttribArray(1);
	::glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)*5, (void*)(sizeof(float)*3));
	::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	::glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);
	::glBindVertexArray(0);

	QImage tex = QImage("assets/textures/cube.png").convertToFormat(QImage::Format_RGBA8888).mirrored();
	::glBindTexture(GL_TEXTURE_2D, m_tex);
	::glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8_ALPHA8, tex.width(), tex.height(), 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, tex.constBits());
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	::glGenerateMipmap(GL_TEXTURE_2D);

	this->m_distance = 10.0;
	this->m_yaw = 0.7;
	this->m_pitch = 0.7;
	this->update_view();
}

gravity::renderer::~renderer()
{
	::glDeleteBuffers(1, &m_ebo);
	::glDeleteBuffers(1, &m_vbo);
	::glDeleteVertexArrays(1, &m_vao);
	::glDeleteTextures(1, &m_tex);
	::glDeleteProgram(m_shader);
}


void gravity::renderer::render()
{
	quint64 now_ns = m_timer->nsecsElapsed();
	quint64 dt_ns = now_ns - m_last_ns;
	this->m_last_ns = now_ns;

	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	::glEnable(GL_DEPTH_TEST);
	::glUseProgram(m_shader);
	int location = -1;
	location = ::glGetUniformLocation(m_shader, "projection");
	::glUniformMatrix4fv(location, 1, GL_FALSE, (m_projection * m_view).cast<float>().eval().data());

	location = ::glGetUniformLocation(m_shader, "model");
	for (int i=0; i<m_gravity_sim->count(); i++) {
		Eigen::Vector3d pos = m_gravity_sim->get_position(i);
		Eigen::Affine3d model= Eigen::Translation3d(pos) * Eigen::Scaling(0.1);
		::glUniformMatrix4fv(location, 1, GL_FALSE, model.matrix().cast<float>().eval().data());
		::glBindVertexArray(m_vao);
		::glBindTexture(GL_TEXTURE_2D, m_tex);
		::glDrawElements(GL_TRIANGLES, sizeof(cube_indices)/sizeof(unsigned int), GL_UNSIGNED_INT, 0);
		::glBindVertexArray(0);
	}

	this->m_gravity_sim->update(dt_ns / 1.0e9);
	this->update();
}
