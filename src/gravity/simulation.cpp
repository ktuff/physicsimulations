#include <stdexcept>

#include "simulation.hpp"


gravity::simulation::simulation()
: m_integration(FORWARD)
, m_mode(SINGULAR)
, m_g_constant(9.81)
, m_timestep(0)
{
	this->m_masses.push_back(mass());
	this->m_positions.push_back(Eigen::Vector3d(0.0, 0.0, 0.0));
	this->m_velocities.push_back(Eigen::Vector3d(0.0, 0.0, 0.0));
	this->m_ms.push_back(1.0);
}


void gravity::simulation::reset()
{
	this->m_timestep = 0;
	for (size_t i=0; i<m_masses.size(); i++) {
		this->m_masses.at(i).set_position(m_positions[i]);
		this->m_masses.at(i).set_velocity(m_velocities[i]);
		this->m_masses.at(i).set_mass(m_ms[i]);
	}
}

void gravity::simulation::set_integration_mode(int index)
{
	if (index < 0 || index > 2) {
		throw std::runtime_error("Invalid index for integration method: " + std::to_string(index));
	}
	switch (index) {
	case 0:
		this->m_integration = FORWARD;
		break;
	case 1:
		this->m_integration = BACKWARD;
		break;
	case 2:
		this->m_integration = VERLET;
		break;
	}
}

void gravity::simulation::set_mode(bool nbody)
{
	if (nbody) {
		this->m_mode = N_BODY;
	} else {
		this->m_mode = SINGULAR;
	}
}

void gravity::simulation::set_g_constant(double g)
{
	this->m_g_constant = g;
}

void gravity::simulation::set_m_constant(double m)
{
	this->m_big_mass.set_mass(m);
}

void gravity::simulation::update(double dt)
{
	if (m_mode == SINGULAR) {
		double gm = m_g_constant * m_big_mass.get_mass();
		auto pos = m_masses[0].get_position();
		auto vel = m_masses[0].get_velocity();

		switch (m_integration) {
		case FORWARD: {
			Eigen::Vector3d acc = simulation::force(m_masses[0].get_position(), gm);
			this->m_masses[0].set_position(pos + dt * vel);
			this->m_masses[0].set_velocity(vel + dt * acc);
			break;
		}
		case BACKWARD: {
			double pos_dot = pos.dot(pos);
			double div = std::pow(pos_dot, 2.5);
			Eigen::Vector3d acc = simulation::force(pos, gm);

			double xx = pos.x()*pos.x();
			double xy = pos.x()*pos.y();
			double xz = pos.x()*pos.z();
			double yy = pos.y()*pos.y();
			double yz = pos.y()*pos.z();
			double zz = pos.z()*pos.z();
			Eigen::Matrix3d A;
			A(0,0) = -2 * xx + yy + zz;
			A(0,1) =  3 * xy;
			A(0,2) =  3 * xz;
			A(1,0) =  3 * xy;
			A(1,1) = -2 * yy + xx + zz;
			A(1,2) =  3 * yz;
			A(2,0) =  3 * xz;
			A(2,1) =  3 * yz;
			A(2,2) = -2 * zz + xx + yy;
			acc += (-A * (gm / div)) * Eigen::Vector3d(dt, dt, dt);
			auto new_vel = vel + dt * acc;
			this->m_masses[0].set_velocity(new_vel);
			this->m_masses[0].set_position(pos + dt * new_vel);
			break;
		}
		case VERLET: {
			auto new_pos = pos;
			if (m_timestep == 0) {
				new_pos = pos + dt * vel + dt * dt / 2.0 * simulation::force(pos, gm);
			} else {
				new_pos = pos * 2.0 - m_prev_pos + dt * dt * simulation::force(pos, gm);
			}
			this->m_masses[0].set_position(new_pos);
			this->m_prev_pos = pos;
			break;
		}
		}
		this->m_timestep++;
	} else if (m_mode == N_BODY) {
		std::vector<Eigen::Vector3d> acc(m_masses.size());
		for (size_t i=0; i<m_masses.size(); i++) {
			acc[i] = simulation::force_n(i);
		}

		for (size_t i=0; i<m_masses.size(); i++) {
			auto pos = m_masses[i].get_position();
			auto vel = m_masses[i].get_velocity();
			this->m_masses[i].set_position(pos + dt * vel);
			this->m_masses[i].set_velocity(vel + dt * acc[i]);
		}
	}
}

void gravity::simulation::add_mass()
{
	this->m_masses.push_back(mass());
	this->m_positions.push_back(Eigen::Vector3d(0.0, 0.0, 0.0));
	this->m_velocities.push_back(Eigen::Vector3d(0.0, 0.0, 0.0));
	this->m_ms.push_back(1.0);
}

void gravity::simulation::rmv_mass(size_t index)
{
	if (index > 0 && index <= m_masses.size()) {
		this->m_masses.erase(m_masses.begin() + index);
		this->m_positions.erase(m_positions.begin() + index);
		this->m_velocities.erase(m_velocities.begin() + index);
		this->m_ms.erase(m_ms.begin() + index);
	}
}

void gravity::simulation::set_position(size_t index, double x, double y, double z)
{
	switch (m_mode) {
	case SINGULAR:
		this->m_positions[0] = Eigen::Vector3d(x, y, z);
		break;
	case N_BODY:
		if (index < m_masses.size()) {
			this->m_positions[index] = Eigen::Vector3d(x, y, z);
		}
		break;
	}
}

void gravity::simulation::set_velocity(size_t index, double x, double y, double z)
{
	switch (m_mode) {
	case SINGULAR:
		this->m_velocities[0] = Eigen::Vector3d(x, y, z);
		break;
	case N_BODY:
		if (index < m_masses.size()) {
			this->m_velocities[index] = Eigen::Vector3d(x, y, z);
		}
		break;
	}
}

void gravity::simulation::set_mass(size_t index, double m)
{
	switch (m_mode) {
	case SINGULAR:
		this->m_ms[0] = m;
		break;
	case N_BODY:
		if (index < m_masses.size()) {
			this->m_ms[index] = m;
		}
		break;
	}
}

int gravity::simulation::count() const
{
	switch (m_mode) {
	case SINGULAR:
		return 2;
	case N_BODY:
		return m_masses.size()-1;
	}
	return 0;
}

Eigen::Vector3d gravity::simulation::get_position(int index) const
{
	switch (m_mode) {
	case SINGULAR:
		if (index == 0) {
			return m_big_mass.get_position();
		} else {
			return m_masses[0].get_position();
		}
	case N_BODY:
		if (index+1 < (int)m_masses.size()) {
			return m_masses.at(index+1).get_position();
		} else {
			return Eigen::Vector3d(0, 0, 0);
		}
	}
	return Eigen::Vector3d(0, 0, 0);
}


Eigen::Vector3d gravity::simulation::force(const Eigen::Vector3d& r, double gm) const
{
	double r_norm = r.norm();
	double r_norm_3 = r_norm * r_norm * r_norm;
	return -(gm / r_norm_3) * r;
}

Eigen::Vector3d gravity::simulation::force_n(size_t index) const
{
	Eigen::Vector3d sum(0.0, 0.0, 0.0);
	auto const& m1 = m_masses.at(index);
	for (size_t index2=0; index2<m_masses.size(); index2++) {
		if (index != index2) {
			auto const& m2 = m_masses.at(index2);
			auto ri = m2.get_position() - m1.get_position();
			double norm = ri.norm();
			sum += (m2.get_mass() / (norm * norm * norm)) * ri;
		}
	}
	return sum * m_g_constant;
}
