#include "mass.hpp"

gravity::mass::mass()
: m_m(1000.0)
, m_r(0.0, 0.0, 0.0)
, m_v(0.0, 0.0, 0.0)
{
}


Eigen::Vector3d gravity::mass::get_position() const
{
	return m_r;
}

Eigen::Vector3d gravity::mass::get_velocity() const
{
	return m_v;
}

double gravity::mass::get_mass() const
{
	return m_m;
}

gravity::mass * gravity::mass::set_position(const Eigen::Vector3d& r)
{
	this->m_r = r;
	return this;
}

gravity::mass * gravity::mass::set_velocity(const Eigen::Vector3d& v)
{
	this->m_v = v;
	return this;
}

gravity::mass * gravity::mass::set_mass(double value)
{
	this->m_m = value;
	return this;
}
