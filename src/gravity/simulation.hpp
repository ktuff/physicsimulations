#pragma once

#include <vector>

#include "mass.hpp"


namespace gravity {

class simulation {

	private:
		mass m_big_mass;
		std::vector<mass> m_masses;
		std::vector<Eigen::Vector3d> m_positions;
		std::vector<Eigen::Vector3d> m_velocities;
		std::vector<double> m_ms;
		enum {
			FORWARD,
			BACKWARD,
			VERLET
		} m_integration;
		enum {
			N_BODY,
			SINGULAR
		} m_mode;
		double m_g_constant;
		size_t m_timestep;
		Eigen::Vector3d m_prev_pos;


	public:
		simulation();
		~simulation() = default;


	public:
		void reset();
		void set_integration_mode(int index);
		void set_mode(bool nbody);
		void set_g_constant(double g);
		void set_m_constant(double m);
		void update(double dt);

		void add_mass();
		void rmv_mass(size_t index);
		void set_position(size_t index, double x, double y, double z);
		void set_velocity(size_t index, double x, double y, double z);
		void set_mass(size_t index, double m);
		int count() const;
		Eigen::Vector3d get_position(int index) const;

	private:
		Eigen::Vector3d force(Eigen::Vector3d const& r, double gm) const;
		Eigen::Vector3d force_n(size_t index) const;
};

} // namespace gravity
