#include <glad/glad.h>
#include <QEvent>
#include <QMouseEvent>
#include <QOpenGLContext>
#include <QWheelEvent>

#include "glwidget.hpp"
#include "renderer.hpp"


GLWidget::GLWidget(QWidget * parent, Qt::WindowFlags f)
: QOpenGLWidget(parent, f)
{
}


void GLWidget::set_renderer(const std::function<std::unique_ptr<renderer> ()>& rfunc)
{
	this->m_rfunc = rfunc;
}

bool GLWidget::event(QEvent* event)
{
	switch (event->type()) {
	case QEvent::MouseButtonPress:
	case QEvent::MouseButtonRelease:
	case QEvent::MouseMove:
		if (m_renderer) {
			this->m_renderer->on_mouse_event(static_cast<QMouseEvent*>(event));
			return true;
		} else {
			return false;
		}
	case QEvent::Wheel:
		if (m_renderer) {
			this->m_renderer->on_wheel_event(static_cast<QWheelEvent*>(event));
			return true;
		} else {
			return false;
		}
	default:
		return QOpenGLWidget::event(event);
	}
}

void GLWidget::on_switch()
{
	if (m_renderer) {
		this->m_renderer->on_switch();
	}
}

void GLWidget::initializeGL()
{
	thread_local QOpenGLContext * gl_context = QOpenGLWidget::context();
	::gladLoadGLLoader([] (char const * name) {
		return reinterpret_cast<void*>(gl_context->getProcAddress(name));
	});

	this->makeCurrent();
	if (m_rfunc) {
		this->m_renderer = m_rfunc();
		this->connect(m_renderer.get(), &renderer::update, this, qOverload<>(&QWidget::update));
	}

	if (this->m_renderer) {
		this->m_renderer->resize(width(), height());
	}
	this->doneCurrent();
}

void GLWidget::paintGL()
{
	if (m_renderer) {
		this->m_renderer->render();
	}
}

void GLWidget::resizeGL(int w, int h)
{
	if (m_renderer) {
		this->m_renderer->resize(w, h);
	}
}
