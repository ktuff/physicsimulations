#pragma once

#include <Eigen/Core>
#include <QObject>
#include <QPoint>

class QElapsedTimer;
class QMouseEvent;
class QWheelEvent;

class renderer : public QObject {
	Q_OBJECT

	protected:
		int m_width;
		int m_height;
		Eigen::Matrix4d m_projection;
		Eigen::Matrix4d m_view;
		enum {
			ORTHOGRAPHIC,
			PERSPECTIVE
		} m_proj_type;
		QElapsedTimer* m_timer;
		quint64 m_last_ns;
		bool m_dragged;
		QPoint m_last_pos;
		double m_yaw;
		double m_pitch;
		double m_distance;


	public:
		renderer(QElapsedTimer* timer);
		virtual ~renderer();


	public:
		void resize(int width, int height);
		void on_switch();
		void on_mouse_event(QMouseEvent* event);
		void on_wheel_event(QWheelEvent* event);
		virtual void render() = 0;

	protected:
		unsigned int create_shader(std::string const& path_v, std::string const& path_f) const;
		Eigen::Matrix4d look_at(Eigen::Vector3d const& pos, Eigen::Vector3d const& target, Eigen::Vector3d const& up) const;
		void update_view();
		void check_error() const;

	private:
		unsigned int compile_shader(const std::string& path, unsigned int type) const;
		Eigen::Matrix4d calc_ortho(int width, int height) const;
		Eigen::Matrix4d calc_persp(int width, int height) const;

	signals:
		void update();
};
