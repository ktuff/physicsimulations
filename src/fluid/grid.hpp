#pragma once

#include <Eigen/Core>

typedef Eigen::Vector2d vec2;

namespace fluid {

class grid {

	public:
		static int const size = 60;

	private:
		double m_v[size][size];
		double m_u[size][size];
		double m_p[size][size];


	public:
		grid();
		~grid() = default;


	public:
		void reset(int mode);
		void update(double dt);
		double ui(int i, int j) const;
		double vi(int i, int j) const;
		double pi(int i, int j) const;
		void ui(int i, int j, double v);
		void vi(int i, int j, double v);
		void pi(int i, int j, double v);
		double u(int i, int j) const;
		double v(int i, int j) const;
		double p(int i, int j) const;
		static double bilinear(double s, double t, double xy, double xy2, double x2y, double x2y2);
};

} // namespace fluid
