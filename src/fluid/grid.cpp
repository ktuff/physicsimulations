#include "grid.hpp"

namespace fluid {
	const float x0 = 29.5f;
	const float y0 = 29.5f;
	const float x1 = 36.5f;
	const float y1 = 36.5f;
	const float x2 = 22.5f;
	const float y2 = 22.5f;
}

fluid::grid::grid()
{
	this->reset(0);
}


void fluid::grid::reset(int mode)
{
	auto vf = [&](double x, double y) {
		vec2 v(0.0, 0.0);
		switch (mode) {
		case 0: {
				// one
				vec2 r(x - x0, y - y0);
				vec2 ro = vec2(r.y(), -r.x());
				v = 0.5 * ro / ro.squaredNorm();
			}
			break;
		case 1: {
				// two, same rotation
				vec2 r1(x - x1, y - y1);
				vec2 ro1 = vec2(r1.y(), -r1.x());
				vec2 v1 = 0.5 * ro1 / ro1.squaredNorm();

				vec2 r2(x - x2, y - y2);
				vec2 ro2 = vec2(r2.y(), -r2.x());
				vec2 v2 = 0.5 * ro2 / ro2.squaredNorm();
				v = vec2(v1 + v2);
			}
			break;
		case 2: {
				// two, different rotation
				vec2 r1(x - x1, y - y1);
				vec2 ro1 = vec2(r1.y(), -r1.x());
				vec2 v1 = 0.5 * ro1 / ro1.squaredNorm();

				vec2 r2(x - x2, y - y2);
				vec2 ro2 = vec2(-r2.y(), r2.x());
				vec2 v2 = 0.5 * ro2 / ro2.squaredNorm();
				v = vec2(v1 + v2);
			}
			break;
		}
		return v;
	};

	for (int j=0; j<size; j++) {
		for (int i=0; i<size; i++) {
			this->m_p[i][j] = 0.0;
			if (i == 0) {
				this->m_u[i][j] = 0.0;
			} else {
				this->m_u[i][j] = vf(i-0.5, j).x();
			}
			if (j == 0) {
				this->m_v[i][j] = 0.0;
			} else {
				this->m_v[i][j] = vf(i, j-0.5).y();
			}
		}
	}
}

double fluid::grid::ui(int i, int j) const
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return 0.0;
	} else {
		return m_u[i][j];
	}
}

double fluid::grid::vi(int i, int j) const
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return 0.0;
	} else {
		return m_v[i][j];
	}
}

double fluid::grid::pi(int i, int j) const
{
	return p(i, j);
}

void fluid::grid::ui(int i, int j, double v)
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return;
	} else {
		this->m_u[i][j] = v;
	}
}

void fluid::grid::vi(int i, int j, double v)
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return;
	} else {
		this->m_v[i][j] = v;
	}
}

void fluid::grid::pi(int i, int j, double v)
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return;
	} else {
		this->m_p[i][j] = v;
	}
}

double fluid::grid::u(int i, int j) const
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return 0.0;
	} else if (i == size-1) {
		return (m_u[i][j] + 0.0) / 2.0;
	} else {
		return (m_u[i][j] + m_u[i+1][j]) / 2.0;
	}
}

double fluid::grid::v(int i, int j) const
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return 0.0;
	} else if (j == size-1) {
		return (m_v[i][j] + 0.0) / 2.0;
	} else {
		return (m_v[i][j] + m_v[i][j+1]) / 2.0;
	}
}

double fluid::grid::p(int i, int j) const
{
	if (i < 0 || j < 0 || i >= size || j >= size) {
		return 0.0;
	} else {
		return m_p[i][j];
	}
}

double fluid::grid::bilinear(double s, double t, double x0, double x1, double x2, double x3)
{
	return s*t*x0 + s*(1.0-t)*x1 + (1.0-s)*t*x2 + (1.0-s)*(1.0-t)*x3;
}
