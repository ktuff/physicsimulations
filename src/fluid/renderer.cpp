#include <glad/glad.h>
#include <Eigen/Geometry>

#include "renderer.hpp"
#include "simulation.hpp"

#define NUM_VERTICES 4
#define NUM_INDICES 6

static float const rect_vertices[NUM_VERTICES*2] = {
	-0.5f, -0.5f,
	 0.5f,  0.5f,
	-0.5f,  0.5f,
	 0.5f, -0.5f
};

static unsigned int const rect_indices[NUM_INDICES] = {
	0, 1, 2,
	0, 3, 1
};


fluid::renderer::renderer(QElapsedTimer* timer, fluid::simulation* fluid_sim)
: ::renderer(timer)
, m_fluid_sim(fluid_sim)
{
	this->m_proj_type = ORTHOGRAPHIC;
	this->m_shader = renderer::create_shader("assets/shaders/fluid.vert", "assets/shaders/fluid.frag");

	const int n = fluid::grid::size;
	this->m_indices.reserve(n * n * 6);
	for (int y=0; y<n; y++) {
		for (int x=0; x<n; x++) {
			for (int i=0; i<NUM_INDICES; i++) {
				this->m_indices.push_back((y*n + x)*NUM_VERTICES + rect_indices[i]);
			}
		}
	}

	::glGenVertexArrays(1, &m_vao);
	::glGenBuffers(1, &m_vbo);
	::glGenBuffers(1, &m_ebo);

	size_t vertex_size = n * n * NUM_VERTICES * 4 * sizeof(float);
	::glBindVertexArray(m_vao);
	::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	::glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(vertex_size), NULL, GL_DYNAMIC_DRAW);
	::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	::glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(m_indices[0]) * m_indices.size()), m_indices.data(), GL_STATIC_DRAW);
	::glBindVertexArray(0);
}

fluid::renderer::~renderer()
{
	::glDeleteBuffers(1, &m_ebo);
	::glDeleteBuffers(1, &m_vbo);
	::glDeleteVertexArrays(1, &m_vao);
	::glDeleteProgram(m_shader);
}


void fluid::renderer::render()
{
	::glDisable(GL_CULL_FACE);
	::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	::glUseProgram(m_shader);

	int loc = -1;
	float value = 0.0f;
	loc = ::glGetUniformLocation(m_shader, "projection");
	::glUniformMatrix4fv(loc, 1, GL_FALSE, m_projection.cast<float>().eval().data());
	{
		const int n = fluid::grid::size;
		std::vector<float> vertices;
		vertices.reserve(n * n * NUM_VERTICES * 4);
		float fac = 1.0f / n;
		float offset = 0.5 * fac - 0.5;
		for (int y = 0; y < n; ++y) {
			for (int x = 0; x < n; ++x) {
				value = m_fluid_sim->get_value(x, y);
				for (int i = 0; i < NUM_VERTICES; ++i) {
					vertices.push_back((x + rect_vertices[2 * i]) * fac + offset);
					vertices.push_back((y + rect_vertices[2 * i + 1]) * fac + offset);
					vertices.push_back(0.0f);
					vertices.push_back(value);
				}
			}
		}

		::glBindVertexArray(m_vao);
		::glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		::glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(vertices[0]) * vertices.size()), NULL, GL_DYNAMIC_DRAW);
		::glBufferSubData(GL_ARRAY_BUFFER, 0, static_cast<GLsizeiptr>(sizeof(vertices[0]) * vertices.size()), vertices.data());
		::glEnableVertexAttribArray(0);
		::glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
		::glBindVertexArray(0);

		double scale = std::min(m_width, m_height);
		Eigen::Affine3d model = Eigen::Affine3d::Identity() * Eigen::Scaling(scale, scale, scale);
		loc = ::glGetUniformLocation(m_shader, "model");
		::glUniformMatrix4fv(loc, 1, GL_FALSE, model.matrix().cast<float>().eval().data());

		::glBindVertexArray(m_vao);
		::glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
		::glBindVertexArray(0);
	}

	this->m_fluid_sim->update();
	this->update();
}
