#pragma once

#include <Eigen/Sparse>

#include "grid.hpp"

namespace fluid {

class simulation {

	public:
		static constexpr double m_rho = 1.4;

	private:
		grid m_grid;
		enum {
			ONE_VORTEX = 0,
			TWO_VORTEX_SAME = 1,
			TWO_VORTEX_DIFF = 2
		} m_mode;
		double m_dt;
		bool m_view_mode;
		Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
		Eigen::SparseMatrix<double> m_mat_p;
		vec2 m_ext[grid::size][grid::size];


	public:
		simulation();
		~simulation() = default;


	public:
		void reset();
		void update();
		void add_velocity(int x, int y, vec2 const& v);
		void set_mode(int mode);
		float get_value(int x, int y) const;
		void toggle_view_mode();
		bool view_mode() const;

	private:
		void advection(double dt);
		void external(double dt);
		void pressure(double dt);
};

} // namespace fluid
