#pragma once

#include "../renderer.hpp"

namespace fluid {

class simulation;

class renderer : public ::renderer {

	private:
		simulation *m_fluid_sim;
		unsigned int m_shader;
		unsigned int m_vao;
		unsigned int m_vbo;
		unsigned int m_ebo;
		std::vector<unsigned int> m_indices;


	public:
		renderer(QElapsedTimer* timer, simulation* fluid_sim);
		~renderer();


	public:
		void render() override;
};

} // namespace fluid

