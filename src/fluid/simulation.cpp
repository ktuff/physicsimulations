#include "simulation.hpp"

typedef Eigen::Triplet<double> triplet;


fluid::simulation::simulation()
: m_mode(ONE_VORTEX)
, m_dt(1.0)
, m_view_mode(false)
, m_mat_p(grid::size*grid::size, grid::size*grid::size)
{
	std::vector<triplet> coeffs;
	for (int j=0; j<grid::size; ++j) {
		for (int i=0; i<grid::size; ++i) {
			int index = j*grid::size + i;
			coeffs.push_back(triplet(index, index, 4.0));

			// non-diagonal
			if (i-1 >= 0) {
				coeffs.push_back(triplet(index, index - 1, -1.0));
			}
			if (i+1 < grid::size) {
				coeffs.push_back(triplet(index, index + 1, -1.0));
			}
			if (j-1 >= 0) {
				coeffs.push_back(triplet(index, index - grid::size, -1.0));
			}
			if (j+1 < grid::size) {
				coeffs.push_back(triplet(index, index + grid::size, -1.0));
			}

			// diagonal
			if((i <= 0 || i+1 >= grid::size) && (j <= 0 || j+1 >= grid::size)) {
				coeffs.push_back(triplet(index, index, 2.0)); // corner
			} else if(i <= 0 || i+1 >= grid::size || j <= 0 || j+1 >= grid::size) {
				coeffs.push_back(triplet(index, index, 3.0)); // border
			} else {
				coeffs.push_back(triplet(index, index, 4.0)); // inside
			}
		}
	}
	this->m_mat_p.setFromTriplets(coeffs.begin(), coeffs.end());
	this->solver.compute(m_mat_p);
	this->reset();
}


void fluid::simulation::reset()
{
	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			this->m_ext[i][j] = vec2(0.0, 0.0);
		}
	}
	this->m_grid.reset(m_mode);
	this->pressure(m_dt);
}

void fluid::simulation::update()
{
	this->advection(m_dt);
	this->external(m_dt);
	this->pressure(m_dt);
}

void fluid::simulation::add_velocity(int x, int y, vec2 const& v)
{
	if (x >= 0 && y >= 0 && x < grid::size && y < grid::size) {
		this->m_ext[x][y] = v;
	}
}

void fluid::simulation::set_mode(int mode)
{
	switch (mode) {
	case 0:
		this->m_mode = ONE_VORTEX;
		break;
	case 1:
		this->m_mode = TWO_VORTEX_SAME;
		break;
	case 2:
		this->m_mode = TWO_VORTEX_DIFF;
		break;
	}
}

float fluid::simulation::get_value(int i, int j) const
{
	if (m_view_mode) {
		return 60 * m_grid.p(i, j);
	} else {
		return 10 * vec2(m_grid.u(i, j), m_grid.v(i, j)).norm();
	}
}

void fluid::simulation::toggle_view_mode()
{
	this->m_view_mode = !m_view_mode;
}

bool fluid::simulation::view_mode() const
{
	return m_view_mode;
}

void fluid::simulation::advection(double dt)
{
	double tmpu[grid::size][grid::size];
	double tmpv[grid::size][grid::size];
	grid& g = m_grid;

	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			vec2 ue(i - 0.5, j);
			vec2 ve(i, j - 0.5);

			vec2 us = ue - dt*vec2(g.ui(i, j), grid::bilinear(0.5, 0.5, g.vi(i-1, j), g.vi(i-1, j+1), g.vi(i  , j),  g.vi(i, j+1)));
			vec2 vs = ve - dt*vec2(grid::bilinear(0.5, 0.5, g.ui(i, j-1), g.ui(i, j), g.ui(i+1, j-1), g.ui(i+1, j)), g.vi(i, j  ));

			int iu = ::floor(us.x() - 0.5) + 1;
			int ju = ::floor(us.y());
			int iv = ::floor(vs.x());
			int jv = ::floor(vs.y() - 0.5) + 1;

			tmpu[i][j] = grid::bilinear(1.0 - (us.x() - (iu - 0.5)), 1.0 - (us.y() - ju), m_grid.ui(iu, ju), m_grid.ui(iu, ju+1), m_grid.ui(iu+1, ju), m_grid.ui(iu+1, ju+1));
			tmpv[i][j] = grid::bilinear(1.0 - (vs.x() - iv), 1.0 - (vs.y() - (jv - 0.5)), m_grid.vi(iv, jv), m_grid.vi(iv, jv+1), m_grid.vi(iv+1, jv), m_grid.vi(iv+1, jv+1));
		}
	}

	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			if (i != 0) {
				this->m_grid.ui(i, j, tmpu[i][j]);
			}
			if (j != 0) {
				this->m_grid.vi(i, j, tmpv[i][j]);
			}
		}
	}
}

void fluid::simulation::external(double dt)
{
	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			if (i != 0) {
				double u = m_grid.ui(i, j) + dt * m_ext[i][j].x() / m_rho;
				this->m_grid.ui(i, j, u);
			}
			if (j != 0) {
				double v = m_grid.vi(i, j) + dt * m_ext[i][j].y() / m_rho;
				this->m_grid.vi(i, j, v);
			}
			this->m_ext[i][j] = vec2(0.0, 0.0);
		}
	}
}

void fluid::simulation::pressure(double dt)
{
	Eigen::VectorXd divergence(grid::size * grid::size);
	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			double du = ((i >= grid::size-1) ? 0.0 : m_grid.ui(i+1,j)) - m_grid.ui(i, j);
			double dv = ((j >= grid::size-1) ? 0.0 : m_grid.vi(i,j+1)) - m_grid.vi(i, j);
			divergence(j*grid::size + i) = du + dv;
		}
	}

	Eigen::VectorXd pn = solver.solve(-(m_rho / dt) * divergence).eval();
	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			this->m_grid.pi(i, j, pn(j*grid::size + i));
		}
	}

	for (int j=0; j<grid::size; j++) {
		for (int i=0; i<grid::size; i++) {
			if (i != 0) {
				double u = m_grid.ui(i, j) - (dt/m_rho) * (m_grid.pi(i, j) - m_grid.pi(i-1, j));
				this->m_grid.ui(i, j, u);
			}
			if (j != 0) {
				double v = m_grid.vi(i, j) - (dt/m_rho) * (m_grid.pi(i, j) - m_grid.pi(i, j-1));
				this->m_grid.vi(i, j, v);
			}
		}
	}
}
