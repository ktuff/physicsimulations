#include <QApplication>

#include "ui_GLMainWindow.h"
#include "window.hpp"
#include "cloth/simulation.hpp"
#include "fluid/simulation.hpp"
#include "gravity/simulation.hpp"
#include "heat/simulation.hpp"

int main(int argc, char** argv)
{
	QApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
	QApplication app(argc, argv);
	app.setApplicationName("KTuff physic simulations");
	app.setApplicationDisplayName(app.translate("main", "KTuff physic simulations"));
	app.setApplicationVersion("1.0");

	QSurfaceFormat surfaceFormat = QSurfaceFormat::defaultFormat();
	surfaceFormat.setVersion(3, 3);
	surfaceFormat.setProfile(QSurfaceFormat::CoreProfile);
	surfaceFormat.setOption(QSurfaceFormat::DebugContext);
	surfaceFormat.setColorSpace(QSurfaceFormat::sRGBColorSpace);
	surfaceFormat.setSamples(4);
	QSurfaceFormat::setDefaultFormat(surfaceFormat);

	cloth::simulation cloth_sim;
	fluid::simulation fluid_sim;
	gravity::simulation gravity_sim;
	heat::simulation heat_sim;
	GLMainWindow window(&cloth_sim, &fluid_sim, &gravity_sim, &heat_sim);
	window.show();

	return app.exec();
}
