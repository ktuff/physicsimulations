#version 330 core

in vec2 v_tex_coord;

out vec4 color;

uniform sampler2D texture0;


void main()
{
	color = texture(texture0, v_tex_coord);
}

