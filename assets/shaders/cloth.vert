#version 330 core

layout (location = 0) in vec4 vertex;

out float value;

uniform mat4 projection;
uniform mat4 model;


void main()
{
	gl_Position = projection * model * vec4(vertex.xyz, 1.0);
	value = vertex.w;
}
