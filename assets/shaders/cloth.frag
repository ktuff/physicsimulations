#version 330 core

in float value;

out vec4 color;


void main()
{
	color = vec4(value, 0.5, 0.5, 1.0);
}
