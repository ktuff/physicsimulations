#version 330 core

layout (location = 0) in vec4 vert;

out float value;

uniform mat4 projection;
uniform mat4 model;


void main()
{
	gl_Position = projection * model * vec4(vert.xyz, 1.0);
	value = vert.w;
}
