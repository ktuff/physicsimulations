#version 330 core

in float value;

out vec4 color;

uniform float maximum;

void main()
{
	float fac = pow(1 - (value/maximum), 20);
    float H = 240 * fac;
    
    float S = 1.0;
    float V = pow(value / maximum, 0.15);
    float hi = floor(H/60.0);
    float f = (H/60.0 - hi);
    float p = V*(1.0-S);
    float q = V*(1.0-S*f);
    float t = V*(1.0-S*(1.0-f));
    if (hi == 1.0) {
        color = vec4(q, V, p, 1.0);
    } else if (hi == 2.0) {
        color = vec4(p, V, t, 1.0);
    } else if (hi == 3.0) {
        color = vec4(p, q, V, 1.0);
    } else if (hi == 4.0) {
        color = vec4(t, p, V, 1.0);
    } else if (hi == 5.0) {
        color = vec4(V, p, q, 1.0);
    } else {
        color = vec4(V, t, p, 1.0);
    }
}


