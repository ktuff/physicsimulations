# Physical simulations

## Features
* gravity simulation (small-big / n bodies)
* 2D heat simulation
* cloth simulation
* 2D fluid simulation

## Libraries used
* [glad](https://github.com/Dav1dde/glad) - openGL bindings
* [Eigen](https://eigen.tuxfamily.org/index.php) - for math functions
* [Qt](https://www.qt.io/) - GUI
